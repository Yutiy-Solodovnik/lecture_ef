﻿using lecture_EF.BLL.Interfaces;
using lecture_EF.DAL.Entites;
using lecture_EF.DAL.Interfaces;
using System.Collections.Generic;

namespace lecture_EF.BLL.Services
{
    public class MyTaskService : IMyTaskService
    {
        IUnitOfWork Database { get; set; }

        public MyTaskService(IUnitOfWork unitOfWork)
        {
            Database = unitOfWork;
        }
        public void CreateTask(MyTask task)
        {
            Database.Tasks.Create(task);
        }

        public void DeleteTask(int id)
        {
            Database.Tasks.Delete(id);
        }

        public void Dispose()
        {
            Database.Dispose();
        }

        public IEnumerable<MyTask> GetAllTasks()
        {
            return Database.Tasks.ReadAll();
        }

        public MyTask GetTask(int id)
        {
            return Database.Tasks.Read(id);
        }

        public void UpdateTask(MyTask task)
        {
            Database.Tasks.Update(task);
        }
    }
}
