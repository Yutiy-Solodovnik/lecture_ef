﻿using lecture_EF.BLL.Interfaces;
using lecture_EF.DAL.Entites;
using lecture_EF.DAL.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace lecture_EF.BLL.Services
{
    public class TeamService : ITeamService
    {
        private QueryBuilder _queryBuilder;
        IUnitOfWork Database { get; set; }

        public TeamService(IUnitOfWork unitOfWork)
        {
            Database = unitOfWork;
            _queryBuilder = new QueryBuilder(unitOfWork);
        }

        public void CreateTeam(Team team)
        {
            Database.Teams.Create(team);
        }

        public void DeleteTeam(int id)
        {
            Database.Teams.Delete(id);
        }
        public void Dispose()
        {
            Database.Dispose();
        }

        public IEnumerable<Team> GetAllTeams()
        {
            return Database.Teams.ReadAll();
        }

        public Team GetTeam(int id)
        {
            return Database.Teams.Read(id);
        }

        public IEnumerable<IGrouping<int?, User>> GetUsersFromTeam()
        {
            return _queryBuilder.GetUsersFromTeam();
        }

        public void UpdateTeam(Team team)
        {
            Database.Teams.Update(team);
        }
    }
}
