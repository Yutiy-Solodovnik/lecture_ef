﻿using lecture_EF.BLL.Interfaces;
using lecture_EF.DAL.Entites;
using lecture_EF.DAL.Interfaces;
using System.Collections.Generic;

namespace lecture_EF.BLL.Services
{
    public class UserService : IUserService
    {
        private QueryBuilder _queryBuilder;
        IUnitOfWork Database { get; set; }

        public UserService(IUnitOfWork unitOfWork)
        {
            Database = unitOfWork;
            _queryBuilder = new QueryBuilder(unitOfWork);
        }

        public void CreateUser(User user)
        {
            Database.Users.Create(user);
        }

        public void DeleteUser(int id)
        {
            Database.Users.Delete(id);
        }

        public void Dispose()
        {
            Database.Dispose();
        }

        public IEnumerable<User> GetAllUsers()
        {
            return Database.Users.ReadAll();
        }

        public User GetUser(int id)
        {
            return Database.Users.Read(id);
        }


        public Dictionary<string, int> GetTasksInProject(int id)
        {
            return _queryBuilder.GetTasksInProject(id);
        }

        public List<MyTask> GetTasksByUser(int id)
        {
            return _queryBuilder.GetTasksByUser(id);
        }

        public IEnumerable<MyTaskInfo> GetFinishedTasksByUser(int id)
        {
            return _queryBuilder.GetFinishedTasksByUser(id);
        }

        public List<User> GetUsersByName()
        {
            return _queryBuilder.GetUsersByName();
        }

        public UserInfo GetUserInfo(int id)
        {
            return _queryBuilder.GetUserInfo(id);
        }
        public void UpdateUser(User user)
        {
            Database.Users.Update(user);
        }
    }
}
