﻿using lecture_EF.BLL.Interfaces;
using lecture_EF.DAL.Entites;
using lecture_EF.DAL.Interfaces;
using System.Collections.Generic;

namespace lecture_EF.BLL.Services
{
    public class ProjectService : IProjectService
    {
        private QueryBuilder _queryBuilder;
        IUnitOfWork Database { get; set; }

        public ProjectService(IUnitOfWork unitOfWork)
        {
            Database = unitOfWork;
            _queryBuilder = new QueryBuilder(unitOfWork);
        }
        public void CreateProject(Project project)
        {
            Database.Projects.Create(project);
        }

        public void DeleteProject(int id)
        {
            Database.Projects.Delete(id);
        }

        public void Dispose()
        {
            Database.Dispose();
        }

        public IEnumerable<Project> GetAllProjects()
        {
            return Database.Projects.ReadAll();
        }

        public Project GetProject(int id)
        {
            return Database.Projects.Read(id);
        }

        public  List<ProjectInfo> GetProjectInfo()
        {
            return _queryBuilder.GetProjectInfo();
        }

        public void UpdateProject(Project project)
        {
            Database.Projects.Update(project);
        }
    }
}
