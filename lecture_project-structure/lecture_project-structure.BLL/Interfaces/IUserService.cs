﻿using lecture_EF.DAL.Entites;
using System.Collections.Generic;

namespace lecture_EF.BLL.Interfaces
{
    public interface IUserService
    {
        void CreateUser(User team);
        IEnumerable<User> GetAllUsers();
        User GetUser(int id);
        void UpdateUser(User user);
        void DeleteUser(int id);
        IEnumerable<MyTaskInfo> GetFinishedTasksByUser(int id);
        List<MyTask> GetTasksByUser(int id);
        Dictionary<string, int> GetTasksInProject(int id);
        List<User> GetUsersByName();
        UserInfo GetUserInfo(int id);
        void Dispose();
    }
}
