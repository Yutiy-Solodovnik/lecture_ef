﻿using lecture_EF.DAL.Entites;
using System.Collections.Generic;

namespace lecture_EF.BLL.Interfaces
{
    public interface IMyTaskService
    {
        void CreateTask(MyTask task);
        IEnumerable<MyTask> GetAllTasks();
        MyTask GetTask(int id);
        void UpdateTask(MyTask task);
        void DeleteTask(int id);
        void Dispose();
    }
}
