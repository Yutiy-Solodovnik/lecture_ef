﻿using lecture_EF.DAL.Entites;
using System.Collections.Generic;

namespace lecture_EF.BLL.Interfaces
{
    public interface IProjectService
    {
        void CreateProject(Project project);
        IEnumerable<Project> GetAllProjects();
        Project GetProject(int id);
        List<ProjectInfo> GetProjectInfo();
        void UpdateProject(Project project);
        void DeleteProject(int id);
        void Dispose();
    }
}
