﻿using lecture_EF.DAL.Entites;
using System.Collections.Generic;
using System.Linq;

namespace lecture_EF.BLL.Interfaces
{
    public interface ITeamService
    {
        void CreateTeam(Team team);
        IEnumerable<Team> GetAllTeams();
        Team GetTeam(int id);
        void UpdateTeam(Team team);
        IEnumerable<IGrouping<int?, User>> GetUsersFromTeam();
        void DeleteTeam(int id);
        void Dispose();
    }
}
