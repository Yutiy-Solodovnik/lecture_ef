﻿using AutoMapper;
using lecture_EF.BLL.DTO;
using lecture_EF.BLL.Interfaces;
using lecture_EF.DAL.Entites;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Threading.Tasks;

namespace WebApi.Projects.Controllers
{
    [Route("api/[controller]")]
    [Produces("application/json")]
    public class ProjectsController : ControllerBase
    {
        private IProjectService _projectService;
        private readonly IMapper _mapper;
        
        public ProjectsController(IMapper mapper, IProjectService projectService)
        {
            _mapper = mapper;
            _projectService = projectService;
        }

        [HttpGet("{id}")]
        public ActionResult<ProjectDTO> GetProject(string id)
        {
            Project project = _projectService.GetProject(Int32.Parse(id));
            ProjectDTO projectDTO = _mapper.Map<ProjectDTO>(project);
            return Ok(projectDTO);
        }

        [HttpGet]
        public ActionResult<IEnumerable<ProjectDTO>> GetALLProjects()
        {
            IEnumerable<Project> projects = _projectService.GetAllProjects();
            IEnumerable<ProjectDTO> projectsDTO = _mapper.Map<IEnumerable<ProjectDTO>>(projects);
            return Ok(projectsDTO);
        }

        [HttpGet("projectInfo")]
        public ActionResult<List<UserDTO>> GetUsersByName()
        {
            List<ProjectInfo> projectInfo = _projectService.GetProjectInfo();
            List<ProjectInfoDTO> projectInfoDTO = _mapper.Map<List<ProjectInfoDTO>>(projectInfo);
            return Ok(projectInfoDTO);
        }

        [HttpPost]
        public ActionResult<ProjectDTO> CreateProject()
        {
            Task<string> todoJson = new StreamReader(Request.Body).ReadToEndAsync();
            ProjectDTO projectDTO = JsonConvert.DeserializeObject<ProjectDTO>(todoJson.Result);
            Project project = _mapper.Map<Project>(projectDTO);
            List<ValidationResult> results = new List<ValidationResult>();
            System.ComponentModel.DataAnnotations.ValidationContext context = new System.ComponentModel.DataAnnotations.ValidationContext(project);
            string errors = null;
            if (!Validator.TryValidateObject(project, context, results, true))
            {
                foreach (var error in results)
                {
                    errors += error.ErrorMessage + ", ";
                }
                return BadRequest(errors);
            }
            else
            {
                _projectService.CreateProject(project);
                return Ok(project);
            }
        }

        [HttpDelete("{id}")]
        public ActionResult<string> DeleteProject(string id)
        {
            _projectService.DeleteProject(Int32.Parse(id));
            return Ok("Deleted");
        }

        [HttpPut]
        public ActionResult<string> UpdateProject()
        {
            Task<string> todoJson = new StreamReader(Request.Body).ReadToEndAsync();
            ProjectDTO projectDTO = JsonConvert.DeserializeObject<ProjectDTO>(todoJson.Result);
            Project project = _mapper.Map<Project>(projectDTO);
            List<ValidationResult> results = new List<ValidationResult>();
            System.ComponentModel.DataAnnotations.ValidationContext context = new System.ComponentModel.DataAnnotations.ValidationContext(project);
            string errors = null;
            if (!Validator.TryValidateObject(project, context, results, true))
            {
                foreach (var error in results)
                {
                    errors += error.ErrorMessage + ", ";
                }
                return BadRequest(errors);
            }
            else
            {
                _projectService.UpdateProject(project);
                return Ok("Updated");
            }
        }
    }
}
