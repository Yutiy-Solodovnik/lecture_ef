﻿using AutoMapper;
using lecture_EF.BLL.DTO;
using lecture_EF.BLL.Interfaces;
using lecture_EF.DAL.Entites;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Threading.Tasks;

namespace WebApi.MyTasks.Controllers
{
    [Route("api/[controller]")]
    [Produces("application/json")]
    public class TasksController : Controller
    {
        private IMyTaskService _myTaskService;
        private readonly IMapper _mapper;

        public TasksController(IMapper mapper, IMyTaskService myTaskService)
        {
            _mapper = mapper;
            _myTaskService = myTaskService;
        }

        [HttpGet("{id}")]
        public ActionResult<MyTaskDTO> GetMyTask(string id)
        {
            MyTask myTask = _myTaskService.GetTask(Int32.Parse(id));
            MyTaskDTO myTaskDTO = _mapper.Map<MyTaskDTO>(myTask);
            return Ok(myTaskDTO);
        }
        
        [HttpGet]
        public ActionResult<IEnumerable<MyTaskDTO>> GetALLMyTasks()
        {
            IEnumerable<MyTask> myTasks = _myTaskService.GetAllTasks();
            IEnumerable<MyTaskDTO> myTasksDTO = _mapper.Map<IEnumerable<MyTaskDTO>>(myTasks);
            return Ok(myTasksDTO);
        }

        [HttpPost]
        public ActionResult<MyTaskDTO> CreateMyTask()
        {
            Task<string> todoJson = new StreamReader(Request.Body).ReadToEndAsync();
            MyTaskDTO myTaskDTO = JsonConvert.DeserializeObject<MyTaskDTO>(todoJson.Result);
            MyTask myTask = _mapper.Map<MyTask>(myTaskDTO);
            List<ValidationResult> results = new List<ValidationResult>();
            System.ComponentModel.DataAnnotations.ValidationContext context = new System.ComponentModel.DataAnnotations.ValidationContext(myTask);
            string errors = null;
            if (!Validator.TryValidateObject(myTask, context, results, true))
            {
                foreach (var error in results)
                {
                    errors += error.ErrorMessage + ", ";
                }
                return BadRequest(errors);
            }
            else
            {
                _myTaskService.CreateTask(myTask);
                return Ok(myTask);
            }
        }

        [HttpDelete("{id}")]
        public ActionResult<string> DeleteMyTask(string id)
        {
            _myTaskService.DeleteTask(Int32.Parse(id));
            return Ok("Deleted");
        }

        [HttpPut]
        public ActionResult<string> UpdateMyTask()
        {
            Task<string> todoJson = new StreamReader(Request.Body).ReadToEndAsync();
            MyTaskDTO myTaskDTO = JsonConvert.DeserializeObject<MyTaskDTO>(todoJson.Result);
            MyTask myTask = _mapper.Map<MyTask>(myTaskDTO);
            List<ValidationResult> results = new List<ValidationResult>();
            System.ComponentModel.DataAnnotations.ValidationContext context = new System.ComponentModel.DataAnnotations.ValidationContext(myTask);
            string errors = null;
            if (!Validator.TryValidateObject(myTask, context, results, true))
            {
                foreach (var error in results)
                {
                    errors += error.ErrorMessage + ", ";
                }
                return BadRequest(errors);
            }
            else
            {
                _myTaskService.UpdateTask(myTask);
                return Ok("Updated");
            }
        }
    }
}
