﻿using AutoMapper;
using lecture_EF.BLL.DTO;
using lecture_EF.BLL.Interfaces;
using lecture_EF.DAL.Entites;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Threading.Tasks;

namespace WebApi.Users.Controllers
{
    [Route("api/[controller]")]
    [Produces("application/json")]
    public class UsersController : Controller
    {
        private IUserService _userService;
        private readonly IMapper _mapper;

        public UsersController(IMapper mapper, IUserService userService)
        {
            _mapper = mapper;
            _userService = userService;
        }

        [HttpPost]
        public ActionResult<UserDTO> CreateUser()
        {
            Task<string> todoJson = new StreamReader(Request.Body).ReadToEndAsync();
            UserDTO userDTO = JsonConvert.DeserializeObject<UserDTO>(todoJson.Result);
            User user = _mapper.Map<User>(userDTO);
            List<ValidationResult> results = new List<ValidationResult>();
            System.ComponentModel.DataAnnotations.ValidationContext context = new System.ComponentModel.DataAnnotations.ValidationContext(user);
            string errors = null;
            if (!Validator.TryValidateObject(user, context, results, true))
            {
                foreach (var error in results)
                {
                    errors += error.ErrorMessage + ", ";
                }
                return BadRequest(errors);
            }
            else
            { 
                _userService.CreateUser(user);
                return Ok(user);
            }
        }

        [HttpGet("{id}")]
        public ActionResult<UserDTO> GetUser(string id)
        {
            User user = _userService.GetUser(Int32.Parse(id));
            UserDTO userDTO = _mapper.Map<UserDTO>(user);
            return Ok(userDTO);
        }

        [HttpGet]
        public ActionResult<IEnumerable<UserDTO>> GetALLUsers()
        {
            IEnumerable<User> users = _userService.GetAllUsers();
            IEnumerable<UserDTO> usersDTO = _mapper.Map<IEnumerable<UserDTO>>(users);
            return Ok(usersDTO);
        }

        [HttpGet("tasksInProjectOfUser/{id}")]
        public ActionResult<Dictionary<string, int>> GetTasksInProject(string id)
        {
            Dictionary<string, int> tasks = _userService.GetTasksInProject(Int32.Parse(id));
            return Ok(tasks);
        }

        [HttpGet("finishedTasksByUser/{id}")]
        public ActionResult<IEnumerable<MyTaskInfoDTO>> GetFinishedTasksByUser(string id)
        {
            IEnumerable<MyTaskInfo> myTasksInfo = _userService.GetFinishedTasksByUser(Int32.Parse(id));
            IEnumerable<MyTaskInfoDTO> myTasksInfoDTO = _mapper.Map<IEnumerable<MyTaskInfoDTO>>(myTasksInfo);
            return Ok(myTasksInfoDTO);
        }

        [HttpGet("tasksByUser/{id}")]
        public ActionResult<List<MyTaskDTO>> GetTasksByUser(string id)
        {
            List<MyTask> tasks = _userService.GetTasksByUser(Int32.Parse(id));
            List<MyTaskDTO> tasksDTO = _mapper.Map<List<MyTaskDTO>>(tasks);
            return Ok(tasksDTO);
        }

        [HttpGet("usersByName")]
        public ActionResult<List<UserDTO>> GetUsersByName()
        {
            List<User> users = _userService.GetUsersByName();
            List<UserDTO> usersDTO = _mapper.Map<List<UserDTO>>(users);
            return Ok(usersDTO);
        }

        [HttpGet("userInfo/{id}")]
        public ActionResult<UserInfoDto> GetUserInfo(string id)
        {
            UserInfo users = _userService.GetUserInfo(Int32.Parse(id));
            UserInfoDto usersDTO = _mapper.Map<UserInfoDto>(users);
            return Ok(usersDTO);
        }

        [HttpDelete("{id}")]
        public ActionResult<string> DeleteUser(string id)
        {
            _userService.DeleteUser(Int32.Parse(id));
            return Ok("Deleted");
        }

        [HttpPut]
        public ActionResult<string> UpdateUser()
        {
            Task<string> todoJson = new StreamReader(Request.Body).ReadToEndAsync();
            UserDTO userDTO = JsonConvert.DeserializeObject<UserDTO>(todoJson.Result);
            User user = _mapper.Map<User>(userDTO);
            List<ValidationResult> results = new List<ValidationResult>();
            System.ComponentModel.DataAnnotations.ValidationContext context = new System.ComponentModel.DataAnnotations.ValidationContext(user);
            string errors = null;
            if (!Validator.TryValidateObject(user, context, results, true))
            {
                foreach (var error in results)
                {
                    errors += error.ErrorMessage + ", ";
                }
                return BadRequest(errors);
            }
            else
            {
                _userService.UpdateUser(user);
                return Ok("Updated");
            }
        }
    }
}
