using lecture_EF.BLL.Interfaces;
using lecture_EF.BLL.Services;
using lecture_EF.DAL.EF;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using lecture_EF.DAL.Interfaces;
using lecture_EF.DAL.Repositories;
using lecture_EF.DAL.Entites;

namespace WebApi.Projects
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddTransient<IUnitOfWork, UnitOfWork>();
            services.AddTransient<IRepository<Project>, ProjectRepository>();
            services.AddTransient<IRepository<MyTask>, MyTaskRepositry>();
            services.AddTransient<IRepository<Team>, TeamRepository>();
            services.AddTransient<IRepository<User>, UserRepository>();
            services.AddTransient<IProjectService, ProjectService>();
            services.AddTransient<IMyTaskService, MyTaskService>();
            services.AddTransient<ITeamService, TeamService>();
            services.AddTransient<IUserService, UserService>();

            services.AddAutoMapper(typeof(Startup));

            services.AddDbContext<ProjectsContext>(options => options.UseSqlServer(Configuration.GetConnectionString("ProjectsDatabaseConnection")));

            services.AddControllers();  
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "WebApi.Projects", Version = "v1" });
            });
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
