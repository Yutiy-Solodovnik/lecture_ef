﻿using System;

namespace lecture_EF.DAL.Entites
{
    public class MyTaskInfo
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
