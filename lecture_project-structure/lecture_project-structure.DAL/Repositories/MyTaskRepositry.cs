﻿using lecture_EF.DAL.EF;
using lecture_EF.DAL.Entites;
using lecture_EF.DAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace lecture_EF.DAL.Repositories
{
    public class MyTaskRepositry : IRepository<MyTask>
    {
        private ProjectsContext db;
        public MyTaskRepositry(ProjectsContext context)
        {
            db = context;
        }
        public void Create(MyTask item)
        {
            db.MyTasks.Add(item);
        }

        public void Delete(int id)
        {
            MyTask task = db.MyTasks.Find(id);
            if (task != null)
                db.MyTasks.Remove(task);
        }

        public MyTask Read(int id)
        {
            return db.MyTasks.Find(id);
        }

        public IEnumerable<MyTask> ReadAll()
        {
            return db.MyTasks.ToList();
        }

        public void Update(MyTask item)
        {
            MyTask task = Read(item.Id);
            if (task != null)
            {
                task.Name = item.Name;
                task.Performer = item.Performer;
                task.PerformerId = item.PerformerId;
                task.ProjectId = item.ProjectId;
                task.State = item.State;
                task.Description = item.Description;
                task.CreatedAt = item.CreatedAt;
                task.FinishedAt = item.FinishedAt;
                db.Entry(task).State = EntityState.Modified;
                db.SaveChanges();
            }
        }
    }
}
