﻿using lecture_EF.DAL.EF;
using lecture_EF.DAL.Entites;
using lecture_EF.DAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace lecture_EF.DAL.Repositories
{
    public class UserRepository : IRepository<User>
    {
        private ProjectsContext db;
        public UserRepository(ProjectsContext context)
        {
            db = context;
        }
        public void Create(User item)
        {
            db.Users.Add(item);
            db.SaveChanges();
        }

        public void Delete(int id)
        {
            User user = db.Users.Find(id);
            if (user != null)
            {
                db.Users.Remove(user);
                db.SaveChanges();
            }
        }

        public User Read(int id)
        {
            return db.Users.Find(id);
        }

        public IEnumerable<User> ReadAll()
        {
            return db.Users.ToList();
        }

        public void Update(User item)
        {
            User user = Read(item.Id);
            if (user != null)
            {
                user.FirstName = item.FirstName;
                user.LastName = item.LastName;
                user.Email = item.Email;
                user.BirthDay = item.BirthDay;
                user.TeamId = item.TeamId;
                user.RegisteredAt = item.RegisteredAt;
                db.Entry(user).State = EntityState.Modified;
                db.SaveChanges();
            }
        }
    }
}
