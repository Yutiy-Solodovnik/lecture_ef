﻿using lecture_EF.DAL.Entites;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;
namespace lecture_project_structure.DAL.EF
{
    public static class ModelBuilderExtention 
    {
        private static Project[] _projects;
        private static MyTask[] _tasks;
        private static Team[] _teams;
        private static User[] _users;
        static ModelBuilderExtention()
        {
            GetProjects();
            GetTasks();
            GetTeams();
            GetUsers();
        }

        private static void GetProjects()
        {
            using (StreamReader r = new StreamReader(@"Seeds\projects.json"))
            {
                string json = r.ReadToEnd();
                _projects = JsonConvert.DeserializeObject<List<Project>>(json).ToArray();
            }
        }

        private static void GetTasks()
        {
            using (StreamReader r = new StreamReader(@"Seeds\tasks.json"))
            {
                string json = r.ReadToEnd();
                _tasks = JsonConvert.DeserializeObject<List<MyTask>>(json).ToArray();
            }
        }

        private static void GetTeams()
        {
            using (StreamReader r = new StreamReader(@"Seeds\teams.json"))
            {
                string json = r.ReadToEnd();
                _teams = JsonConvert.DeserializeObject<List<Team>>(json).ToArray();
            }
        }

        private static void GetUsers()
        {
            using (StreamReader r = new StreamReader(@"Seeds\users.json"))
            {
                string json = r.ReadToEnd();
                _users = JsonConvert.DeserializeObject<List<User>>(json).ToArray();
            }
        }

        public static void Seed(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Project>().HasData(_projects);
            modelBuilder.Entity<MyTask>().HasData(_tasks);
            modelBuilder.Entity<Team>().HasData(_teams);
            modelBuilder.Entity<User>().HasData(_users);
        }
    }
}
