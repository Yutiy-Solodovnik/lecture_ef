﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace lecture_project_structure.DAL.Migrations
{
    public partial class AddSeeds : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MyTasks_Projects_ProjectId",
                table: "MyTasks");

            migrationBuilder.DropForeignKey(
                name: "FK_MyTasks_Users_PerformerId",
                table: "MyTasks");

            migrationBuilder.DropForeignKey(
                name: "FK_Projects_Teams_TeamId",
                table: "Projects");

            migrationBuilder.DropForeignKey(
                name: "FK_Projects_Users_AuthorId",
                table: "Projects");

            migrationBuilder.InsertData(
                table: "Teams",
                columns: new[] { "Id", "CreatedAt", "Name" },
                values: new object[,]
                {
                    { 1, new DateTime(2019, 8, 25, 18, 48, 6, 62, DateTimeKind.Local).AddTicks(3310), "Denesik - Greenfelder" },
                    { 2, new DateTime(2017, 3, 31, 5, 29, 28, 374, DateTimeKind.Local).AddTicks(504), "Durgan Group" },
                    { 3, new DateTime(2019, 2, 21, 17, 47, 30, 379, DateTimeKind.Local).AddTicks(7852), "Kassulke LLC" },
                    { 4, new DateTime(2018, 8, 28, 11, 18, 46, 416, DateTimeKind.Local).AddTicks(342), "Harris LLC" },
                    { 5, new DateTime(2019, 4, 3, 12, 58, 33, 17, DateTimeKind.Local).AddTicks(8179), "Mitchell Inc" },
                    { 6, new DateTime(2016, 10, 5, 10, 57, 2, 842, DateTimeKind.Local).AddTicks(7653), "Smitham Group" }
                });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[,]
                {
                    { 9, new DateTime(1963, 9, 19, 20, 24, 20, 711, DateTimeKind.Local).AddTicks(674), "Lori_Vandervort@hotmail.com", "Lori", "Vandervort", new DateTime(2018, 9, 1, 1, 41, 7, 688, DateTimeKind.Local).AddTicks(8922), 1 },
                    { 8, new DateTime(1981, 1, 10, 13, 38, 30, 529, DateTimeKind.Local).AddTicks(222), "Christie.Gusikowski@hotmail.com", "Christie", "Gusikowski", new DateTime(2019, 10, 7, 10, 41, 43, 246, DateTimeKind.Local).AddTicks(699), 6 },
                    { 7, new DateTime(1962, 2, 26, 10, 8, 59, 71, DateTimeKind.Local).AddTicks(1820), "Ann.Langworth@hotmail.com", "Ann", "Langworth", new DateTime(2017, 8, 9, 8, 47, 42, 369, DateTimeKind.Local).AddTicks(9036), 2 },
                    { 6, new DateTime(2009, 5, 12, 5, 9, 16, 237, DateTimeKind.Local).AddTicks(3321), "Joanna25@hotmail.com", "Joanna", "Botsford", new DateTime(2019, 5, 14, 21, 2, 44, 99, DateTimeKind.Local).AddTicks(5821), 2 },
                    { 3, new DateTime(2007, 1, 1, 7, 10, 18, 898, DateTimeKind.Local).AddTicks(8690), "Brandy.Witting@gmail.com", "Brandy", "Witting", new DateTime(2019, 1, 10, 4, 51, 56, 714, DateTimeKind.Local).AddTicks(8896), 3 },
                    { 4, new DateTime(1980, 2, 20, 18, 32, 12, 635, DateTimeKind.Local).AddTicks(8667), "Theresa82@hotmail.com", "Theresa", "Ebert", new DateTime(2017, 5, 14, 5, 37, 44, 448, DateTimeKind.Local).AddTicks(6766), 3 },
                    { 10, new DateTime(1994, 5, 9, 11, 53, 45, 921, DateTimeKind.Local).AddTicks(8955), "Micheal71@hotmail.com", "Micheal", "Franecki", new DateTime(2020, 7, 6, 22, 33, 41, 938, DateTimeKind.Local).AddTicks(340), 5 },
                    { 2, new DateTime(1992, 9, 27, 23, 27, 3, 523, DateTimeKind.Local).AddTicks(6015), "Theresa_Gottlieb66@yahoo.com", "Theresa", "Gottlieb", new DateTime(2019, 12, 4, 18, 52, 4, 372, DateTimeKind.Local).AddTicks(7619), 3 },
                    { 1, new DateTime(1953, 12, 23, 4, 31, 55, 625, DateTimeKind.Local).AddTicks(1180), "Vivian99@yahoo.com", "Vivian", "Mertz", new DateTime(2018, 10, 19, 1, 37, 40, 756, DateTimeKind.Local).AddTicks(2662), 4 },
                    { 5, new DateTime(1954, 4, 9, 8, 4, 50, 470, DateTimeKind.Local).AddTicks(9098), "Alfredo_Simonis@yahoo.com", "Alfredo", "Simonis", new DateTime(2019, 10, 14, 19, 40, 52, 277, DateTimeKind.Local).AddTicks(2028), 2 },
                    { 11, new DateTime(1961, 9, 11, 8, 38, 14, 838, DateTimeKind.Local).AddTicks(2487), "Felicia.Kirlin74@yahoo.com", "Felicia", "Kirlin", new DateTime(2020, 12, 5, 22, 40, 46, 113, DateTimeKind.Local).AddTicks(3753), 5 }
                });

            migrationBuilder.InsertData(
                table: "Projects",
                columns: new[] { "Id", "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[,]
                {
                    { 1, 5, new DateTime(2019, 7, 17, 9, 42, 48, 376, DateTimeKind.Local).AddTicks(2460), new DateTime(2021, 8, 3, 4, 8, 10, 322, DateTimeKind.Local).AddTicks(8394), "Repellendus expedita dolorum saepe ut culpa nobis sunt itaque labore.", "open architecture Outdoors, Grocery & Baby Dynamic", 6 },
                    { 2, 6, new DateTime(2020, 8, 25, 20, 49, 50, 451, DateTimeKind.Local).AddTicks(8054), new DateTime(2021, 9, 12, 22, 17, 47, 233, DateTimeKind.Local).AddTicks(5223), "Et doloribus et temporibus.", "backing up Handcrafted Fresh Shoes challenge", 3 },
                    { 3, 7, new DateTime(2021, 1, 30, 16, 38, 53, 883, DateTimeKind.Local).AddTicks(8745), new DateTime(2021, 7, 24, 15, 7, 31, 935, DateTimeKind.Local).AddTicks(7846), "Non voluptatem voluptas libero.", "Village", 4 },
                    { 6, 8, new DateTime(2021, 3, 15, 21, 47, 35, 933, DateTimeKind.Local).AddTicks(5401), new DateTime(2021, 9, 14, 5, 53, 20, 800, DateTimeKind.Local).AddTicks(3038), "Voluptatibus error ut id libero quam natus molestias natus.", "Libyan Dinar Netherlands Antilles", 2 },
                    { 5, 9, new DateTime(2019, 7, 3, 6, 39, 1, 997, DateTimeKind.Local).AddTicks(8679), new DateTime(2021, 6, 25, 14, 25, 0, 711, DateTimeKind.Local).AddTicks(1264), "Soluta non sed assumenda.", "iterate project", 1 },
                    { 4, 10, new DateTime(2020, 3, 15, 22, 33, 15, 673, DateTimeKind.Local).AddTicks(1141), new DateTime(2021, 7, 21, 11, 32, 51, 335, DateTimeKind.Local).AddTicks(4654), "Quia et tempora hic pariatur voluptatem doloribus sunt.", "Dam", 1 }
                });

            migrationBuilder.InsertData(
                table: "MyTasks",
                columns: new[] { "Id", "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { 1, new DateTime(2017, 10, 31, 11, 39, 16, 990, DateTimeKind.Local).AddTicks(799), "Eveniet nihil asperiores esse minima.", null, "index", 5, 1, 2 });

            migrationBuilder.InsertData(
                table: "MyTasks",
                columns: new[] { "Id", "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { 2, new DateTime(2020, 5, 16, 1, 50, 46, 86, DateTimeKind.Local).AddTicks(832), "Quo sint aut et ea voluptatem omnis ut.", null, "real-time", 4, 2, 2 });

            migrationBuilder.InsertData(
                table: "MyTasks",
                columns: new[] { "Id", "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { 3, new DateTime(2019, 2, 15, 17, 6, 52, 60, DateTimeKind.Local).AddTicks(666), "Eum a eum.", null, "product Direct utilize", 3, 2, 2 });

            migrationBuilder.AddForeignKey(
                name: "FK_MyTasks_Projects_ProjectId",
                table: "MyTasks",
                column: "ProjectId",
                principalTable: "Projects",
                principalColumn: "Id",
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                name: "FK_MyTasks_Users_PerformerId",
                table: "MyTasks",
                column: "PerformerId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                name: "FK_Projects_Teams_TeamId",
                table: "Projects",
                column: "TeamId",
                principalTable: "Teams",
                principalColumn: "Id",
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                name: "FK_Projects_Users_AuthorId",
                table: "Projects",
                column: "AuthorId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.NoAction);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MyTasks_Projects_ProjectId",
                table: "MyTasks");

            migrationBuilder.DropForeignKey(
                name: "FK_MyTasks_Users_PerformerId",
                table: "MyTasks");

            migrationBuilder.DropForeignKey(
                name: "FK_Projects_Teams_TeamId",
                table: "Projects");

            migrationBuilder.DropForeignKey(
                name: "FK_Projects_Users_AuthorId",
                table: "Projects");

            migrationBuilder.DeleteData(
                table: "MyTasks",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "MyTasks",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "MyTasks",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.AddForeignKey(
                name: "FK_MyTasks_Projects_ProjectId",
                table: "MyTasks",
                column: "ProjectId",
                principalTable: "Projects",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_MyTasks_Users_PerformerId",
                table: "MyTasks",
                column: "PerformerId",
                principalTable: "Users",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Projects_Teams_TeamId",
                table: "Projects",
                column: "TeamId",
                principalTable: "Teams",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Projects_Users_AuthorId",
                table: "Projects",
                column: "AuthorId",
                principalTable: "Users",
                principalColumn: "Id");
        }
    }
}
