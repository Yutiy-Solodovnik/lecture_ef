﻿using lecture_EF.DAL.Entites;
using System;

namespace lecture_EF.DAL.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        IRepository<MyTask> Tasks { get; }
        IRepository<Project> Projects { get; }
        IRepository<Team> Teams { get; }
        IRepository<User> Users { get; }

        void Save();
    }
}
