﻿using lecture_EF.BLL.DTO;
using lecture_EF.DAL.Entites;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace lecture_EF.UI
{
    public static class JsonReader
    {
        private static string _baseUri = "https://localhost:44300/";
        public static List<T> GetEntitiesList<T>() where T : class
        {
            using (HttpClient client = new HttpClient())
            {
                string path = typeof(T).Name switch
                {
                    "Project" => "api/Projects",
                    "User" => "api/Users",
                    "Team" => "api/Teams",
                    "MyTask" => "api/Tasks",
                    _ => throw new ArgumentException("Wrong type")
                };
                client.BaseAddress = new Uri(_baseUri);
                Task<HttpResponseMessage> response = client.GetAsync(path);
                response.Wait();
                Task<string> message = response.Result.Content.ReadAsStringAsync();
                message.Wait();
                List<T> entetiesList = JsonConvert.DeserializeObject<List<T>>(message.Result);
                return entetiesList;
            }
        }

        public static void AddUser(UserDTO user)
        {
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(_baseUri);
                Task<HttpResponseMessage> response = client.PostAsync($"api/Users", new StringContent(JsonConvert.SerializeObject(user)));
            }
        }

        public static Dictionary<string, int> GetTasksInProject(int id)
        {
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(_baseUri);
                Task<HttpResponseMessage> response = client.GetAsync($"api/Users/tasksInProjectOfUser/{id}");
                response.Wait();
                Task<string> message = response.Result.Content.ReadAsStringAsync();
                message.Wait();
                Dictionary<string, int> entetiesList = JsonConvert.DeserializeObject<Dictionary<string, int>>(message.Result);
                return entetiesList;
            }
        }

        public static List<MyTaskDTO> GetTasksByUser(int id)
        {
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(_baseUri);
                Task<HttpResponseMessage> response = client.GetAsync($"api/Users/tasksByUser/{id}");
                response.Wait();
                Task<string> message = response.Result.Content.ReadAsStringAsync();
                message.Wait();
                List<MyTaskDTO> entetiesList = JsonConvert.DeserializeObject<List<MyTaskDTO>>(message.Result);
                return entetiesList;
            }
        }
        public static List<MyTaskInfo> GetFinishedTasksByUser(int id)
        {
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(_baseUri);
                Task<HttpResponseMessage> response = client.GetAsync($"api/Users/finishedTasksByUser/{id}");
                response.Wait();
                Task<string> message = response.Result.Content.ReadAsStringAsync();
                message.Wait();
                List<MyTaskInfo> entetiesList = JsonConvert.DeserializeObject<List<MyTaskInfo>>(message.Result);
                return entetiesList;
            }
        }

        public static IEnumerable<IGrouping<int?, User>> GetUsersFromTeam()
        {
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(_baseUri);
                Task<HttpResponseMessage> response = client.GetAsync("api/Teams/usersFromTeams");
                response.Wait();
                Task<string> message = response.Result.Content.ReadAsStringAsync();
                message.Wait();
                IEnumerable<IGrouping<int?, User>> entetiesList = JsonConvert.DeserializeObject<IEnumerable<IGrouping<int?, User>>>(message.Result);
                return entetiesList;
            }
        }

        public static List<User> GetUsersByName()
        {
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(_baseUri);
                Task<HttpResponseMessage> response = client.GetAsync("api/Users/usersByName");
                response.Wait();
                Task<string> message = response.Result.Content.ReadAsStringAsync();
                message.Wait();
                List<User> entetiesList = JsonConvert.DeserializeObject<List<User>>(message.Result);
                return entetiesList;
            }
        }

        public static UserInfoDto GetUserInfo(int id)
        {
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(_baseUri);
                Task<HttpResponseMessage> response = client.GetAsync($"api/Users/userInfo/{id}");
                response.Wait();
                Task<string> message = response.Result.Content.ReadAsStringAsync();
                message.Wait();
                UserInfoDto entetiesList = JsonConvert.DeserializeObject<UserInfoDto>(message.Result);
                return entetiesList;
            }
        }

        public static List<ProjectInfoDTO> GetProjectInfo()
        {
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(_baseUri);
                Task<HttpResponseMessage> response = client.GetAsync("api/Projects/projectInfo");
                response.Wait();
                Task<string> message = response.Result.Content.ReadAsStringAsync();
                message.Wait();
                List<ProjectInfoDTO> entetiesList = JsonConvert.DeserializeObject<List<ProjectInfoDTO>>(message.Result);
                return entetiesList;
            }
        }

        public static void CreateUser()
        {
            
        }
    }
}
