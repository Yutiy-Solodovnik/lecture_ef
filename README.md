## Requirements:
Instead of storing objects in memory, you need to connect a database   
Add an Initial migration that will create a base with the original structure   
Add two migrations with any changes, be it link changes or renaming columns   
Add seeding   
Add basic model validation using DataAnnotations or FluentAPI   